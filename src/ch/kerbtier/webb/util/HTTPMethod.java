package ch.kerbtier.webb.util;

public enum HTTPMethod {
  POST, GET, UNDEFINED
}
